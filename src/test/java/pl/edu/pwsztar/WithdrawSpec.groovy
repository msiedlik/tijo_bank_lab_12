package pl.edu.pwsztar

import spock.lang.Specification

class WithdrawSpec extends Specification {

    def "should withdraw money if there is sufficient amount on the account"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        def amount = 123
        bank.deposit(accountNumber, amount)

        when: "wants to withdraw money"
        def isWithdrawn = bank.withdraw(accountNumber, amount)

        then: "money is withdrawn"
        isWithdrawn
    }

    def "should not withdraw money if there is no sufficient amount on the account"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        bank.deposit(accountNumber, 100)

        when: "wants to withdraw money"
        def isWithdrawn = bank.withdraw(accountNumber, 200)

        then: "money is not withdrawn"
        !isWithdrawn
    }
}
