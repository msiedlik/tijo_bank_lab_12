package pl.edu.pwsztar

import spock.lang.Specification

class DeleteAccountSpec extends Specification {

    def "should return proper account balance deletion"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        def amount = 421
        bank.deposit(accountNumber, amount)

        when: "the account is deleted"
        def balance = bank.deleteAccount(accountNumber)

        then: "return proper account balance"
        balance == amount
    }

    def "should not delete account if user does not exist"() {
        given: "initial data"
        def bank = new Bank()

        when: "the account is deleted"
        def number = bank.deleteAccount(9887)

        then: "return error code"
        number == BankOperation.ACCOUNT_NOT_EXISTS
    }
}
