package pl.edu.pwsztar

import spock.lang.Specification

class TransferSpec extends Specification {

    def "should transfer money if there is sufficient amount on account"() {
        given: "initial data"
        def bank = new Bank()
        def firstAccountNumber = bank.createAccount()
        def secondAccountNumber = bank.createAccount()
        bank.deposit(firstAccountNumber, 100)

        when: "wants to transfer money"
        def isSuccessful = bank.transfer(firstAccountNumber, secondAccountNumber, 50)

        then: "money is transferred"
        isSuccessful
    }

    def "should not transfer money if there is no sufficient amount on account"() {
        given: "initial data"
        def bank = new Bank()
        def firstAccountNumber = bank.createAccount()
        def secondAccountNumber = bank.createAccount()
        bank.deposit(firstAccountNumber, 50)

        when: "wants to transfer money"
        def isSuccessful = bank.transfer(firstAccountNumber, secondAccountNumber, 120)

        then: "money is not transferred"
        !isSuccessful
    }

    def "should not transfer money if account from which the transfer is being sent does not exist"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()

        when: "wants to transfer money"
        def isSuccessful = bank.transfer(123, accountNumber, 100)

        then: "money is transferred"
        !isSuccessful
    }

    def "should not transfer money if account that receives money does not exist"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        bank.deposit(accountNumber, 100)

        when: "wants to transfer money"
        def isSuccessful = bank.transfer(accountNumber, 123, 100)

        then: "money is transferred"
        !isSuccessful
    }
}
