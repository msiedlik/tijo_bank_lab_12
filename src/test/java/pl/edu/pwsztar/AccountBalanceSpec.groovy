package pl.edu.pwsztar

import spock.lang.Specification

class AccountBalanceSpec extends Specification {

    def "should return proper account balance"() {
        given: "initial data"
        def bank = new Bank()
        def accountNumber = bank.createAccount()
        def amount = 100
        bank.deposit(accountNumber, amount)

        when: "wants to get account balance"
        def balance = bank.accountBalance(accountNumber)

        then: "gets proper balance"
        amount == balance
    }
}
