package pl.edu.pwsztar.repository;

import pl.edu.pwsztar.entity.Account;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {

    private static int accountNumber = 1;

    private final HashMap<Integer, Account> accounts = new HashMap<>();

    public Account create() {
        int number = accountNumber++;
        Account account = new Account(number, 0);
        accounts.put(number, account);
        return account;
    }

    public void delete(int accountNumber) {
        accounts.remove(accountNumber);
    }

    @Override
    public Optional<Account> getByNumber(int accountNumber) {
        return Optional.ofNullable(accounts.get(accountNumber));
    }

    @Override
    public Collection<Account> getAll() {
        return accounts.values();
    }
}
